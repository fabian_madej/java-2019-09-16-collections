package fabian.task;

import fabian.task.model.Dragon;
import fabian.task.model.DragonEgg;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.*;

public class DragonCatalog {

    //list of dragons
    private Collection<Dragon> dragons = new ArrayList<Dragon>();


    //add dragon to list
    public void addDragon(Dragon dragon){
        dragons.add(dragon);
    }

    //remove dragon from list
    public void removeDragon(Dragon dragon){
        dragons.remove(dragon);
    }

    //task 2.1 show all dragons
    public void showAll(){
        Stream<Dragon> stream = dragons.stream();
        stream.forEach(value -> System.out.println(value.toString()));
    }

    //task 2.2 show all names of dragons
    public void showNames() {
        Stream<Dragon> stream = dragons.stream();
        stream.forEach(dragon -> System.out.println(dragon.getName()));
    }

    //task 2.3 show all dragons name and color sorted by name
    public void showName_andColor() {
        Stream<Dragon> stream = dragons.stream();
        stream
                .sorted(new NameComparator())
                .forEach(dragon -> System.out.println(dragon.getName()+" "+dragon.getColor()));
    }

    //task 3.1 return the oldest dragon
    public String theOldest() {
        Dragon maxByAge = dragons
                .stream()
                .max(Comparator.comparing(Dragon::getAge))
                .orElseThrow(NoSuchElementException::new);
        return maxByAge.toString();
    }

    //task 3.2 return the biggest dragon
    public int theBiggest() {
        Dragon maxByWingspan = dragons
                .stream()
                .max(Comparator.comparing(Dragon::getWingspan))
                .orElseThrow(NoSuchElementException::new);
        return maxByWingspan.getWingspan();
    }

    //task 3.3 return length of the longest name of dragon
    public int theLongestName() {
        Dragon maxByName = dragons
                .stream()
                .max(Comparator.comparing(Dragon::getName))
                .orElseThrow(NoSuchElementException::new);
        return maxByName.getName().length();
    }

    //task 4.1 return list of dragons in the same color as parameter of function
    public Collection<Dragon> oneDragonColor(Dragon.Color color) {
        Stream<Dragon> stream = dragons.stream();
        Collection<Dragon> oneColorList = new ArrayList<Dragon>();
        stream.forEach(dragon -> {
            if(dragon.getColor().equals(color)){
                oneColorList.add(dragon);
            }
        });
        return oneColorList;
    }

    //task 4.2 return list with names of dragons
    public Collection<String> onlyNames() {
        Stream<Dragon> stream = dragons.stream();
        Collection<String> onlyNamesOfDragons = new ArrayList<String>();
        stream.forEach(dragon -> onlyNamesOfDragons.add(dragon.getName()));
        return onlyNamesOfDragons;
    }

    //task 4.3 return list with colors of dragons
    public Collection<Dragon.Color> onlyColors() {
        Stream<Dragon> stream = dragons.stream();
        Collection<Dragon.Color> onlyColorsOfDragons = new ArrayList<Dragon.Color>();
        stream.forEach(dragon -> onlyColorsOfDragons.add(dragon.getColor()));
        return onlyColorsOfDragons;
    }

    //task 5.1 return sorted list with dragons
    public Collection<Dragon> sortedList() {
        Stream<Dragon> stream = dragons.stream();
        Collection<Dragon> sortedDragons = new ArrayList<Dragon>();
        stream
                .sorted(new NameComparator())
                .sorted(Comparator.comparingInt(Dragon::getWingspan))
                .sorted(Comparator.comparingInt(Dragon::getAge))
                .sorted(new ColorComparator())
                .forEach(sortedDragons::add);
        return sortedDragons;
    }

    //task 5.2 return list with dragons sorted by age
    public Collection<Dragon> sortedListByAge() {
        Stream<Dragon> stream = dragons.stream();
        Collection<Dragon> sortedDragonsByAge = new ArrayList<Dragon>();
        stream
                .sorted(Comparator.comparingInt(Dragon::getAge))
                .forEach(sortedDragonsByAge::add);
        return sortedDragonsByAge;
    }

    //task 6.1 return true if every dragon is older than parameter from function
    public boolean olderThan(int age) {
        AtomicBoolean result = new AtomicBoolean(true);
        Stream<Dragon> stream = dragons.stream();
        stream.forEach(dragon ->{
            if(dragon.getAge()<age){
                result.set(false);
            }
        });
        return result.get();
    }

    //task 6.2 return true if there is any dragon in color from parameter from function
    public boolean anyColor(Dragon.Color color) {
        AtomicBoolean result = new AtomicBoolean(false);
        Stream<Dragon> stream = dragons.stream();
        stream.forEach(dragon ->{
            if(dragon.getColor().equals(color)){
                result.set(true);
            }
        });
        return result.get();
    }

    //task 7 return list of eggs from all dragons
    public Collection<DragonEgg> allEggs() {
        Stream<Dragon> stream = dragons.stream();
        Collection<DragonEgg> dragonEggs = new ArrayList<>();
        stream.forEach(dragon -> {
            dragonEggs.add(new DragonEgg(dragon));
        });
        return dragonEggs;
    }

    //task 8 return list of eggs from dragons bigger than parameter from function
    public Collection<DragonEgg> EggsFromBigDragon( int wingspan ) {
        Stream<Dragon> stream = dragons.stream();
        Collection<DragonEgg> dragonEggs = new ArrayList<>();
        stream.forEach(dragon -> {
            if(dragon.getWingspan()>=wingspan) {
                dragonEggs.add(new DragonEgg(dragon));
            }
        });
        return dragonEggs;
    }
}
//used tu sort by name
class NameComparator implements Comparator<Dragon> {
    @Override
    public int compare(Dragon o1, Dragon o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
//used to sort by color
class ColorComparator implements Comparator<Dragon> {
    @Override
    public int compare(Dragon o1, Dragon o2) {
        return o1.getColor().compareTo(o2.getColor());
    }
}
