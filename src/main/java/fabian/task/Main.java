package fabian.task;

import fabian.task.model.Dragon;
import fabian.task.model.DragonEgg;

public class Main {
    public static void main(String[] args) {

        DragonCatalog dragonCatalog = new DragonCatalog();

        dragonCatalog.addDragon(new Dragon(Dragon.Color.RED, 100, 200, "dragon_first"));
        dragonCatalog.addDragon(new Dragon(Dragon.Color.BLACK, 1000, 300, "dragon_theBest"));
        dragonCatalog.addDragon(new Dragon(Dragon.Color.WHITE, 50, 100, "dragon_last"));

        //task 2.1
        System.out.println("Show all dragons");
        dragonCatalog.showAll();

        //task 2.2
        System.out.println("---\nShow names of dragons");
        dragonCatalog.showNames();

        //task 2.3
        System.out.println("---\nShow names and colors");
        dragonCatalog.showName_andColor();

        //task 3.1
        System.out.println("---\nThe oldest dragon:");
        System.out.println(dragonCatalog.theOldest());

        //task 3.2
        System.out.println("---\nThe biggest dragon");
        System.out.println("Wingspan: "+dragonCatalog.theBiggest());

        //task 3.3
        System.out.println("---\nThe longest name have:");
        System.out.println(dragonCatalog.theLongestName()+" length!");

        //task 4.1
        System.out.println("---\nShow all red dragons");
        for(Dragon dragon : dragonCatalog.oneDragonColor(Dragon.Color.RED)){
            System.out.println(dragon.toString());
        }

        //task 4.2
        System.out.println("---\nList of names");
        for(String dragon : dragonCatalog.onlyNames()){
            System.out.println(dragon);
        }

        //task 4.3
        System.out.println("---\nList of colors");
        for(Dragon.Color color : dragonCatalog.onlyColors()){
            System.out.println(color);
        }

        //task 5.1
        System.out.println("---\nNew list sorted");
        for(Dragon dragon : dragonCatalog.sortedList()){
            System.out.println(dragon);
        }

        //task 5.2
        System.out.println("---\nNew list sorted by Age");
        for(Dragon dragon : dragonCatalog.sortedListByAge()){
            System.out.println(dragon);
        }

        //task 6.1
        System.out.println("---\nEvery dragon older than 50?");
        System.out.println(dragonCatalog.olderThan(50));

        //task 6.2
        System.out.println("---\nAny dragon is white?");
        System.out.println(dragonCatalog.anyColor(Dragon.Color.WHITE));

        //task 7
        System.out.println("---\nEggs from all dragons");
        for(DragonEgg dragonEgg : dragonCatalog.allEggs()){
            System.out.println(dragonEgg);
        }

        //task 8
        System.out.println("---\nEggs from dragons bigger than 200 wingspan");
        for(DragonEgg dragonEgg : dragonCatalog.EggsFromBigDragon(200)){
            System.out.println(dragonEgg);
        }
    }
}
