package fabian.task.model;

public class DragonEgg {
    private Dragon.Color color;

    //add new egg
    public DragonEgg(Dragon dragon){

        this.color=dragon.getColor();

    }

    public String toString() {
        return "Egg with "+color+" dragon";
    }
}
