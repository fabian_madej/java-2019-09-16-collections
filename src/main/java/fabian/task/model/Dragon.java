package fabian.task.model;

public class Dragon {

    //dragons possible colors
    public enum Color{
        BLACK,
        RED,
        WHITE
    }

    //dragons properties
    private Color color;
    private int age;
    private int wingspan;
    private String name;

    //add new dragon with parameters
    public Dragon(Color color, int age, int wingspan, String name){

        setColor(color);
        setAge(age);
        setWingspan(wingspan);
        setName(name);

    }

    //add new dragon without parameters
    public Dragon(){

    }

    //Setters
    public void setColor(Color color) {
        this.color = color;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    public void setName(String name) {
        this.name = name;
    }

    //Getters
    public Color getColor() {
        return color;
    }

    public int getAge() {
        return age;
    }

    public int getWingspan() {
        return wingspan;
    }

    public String getName() {
        return name;
    }

    public String toString(){
        return  "Dragon : color: "+getColor()+" age: "+getAge()+" wingspan: "+getWingspan()+" name: "+getName();
    }
}
